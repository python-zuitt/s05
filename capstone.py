from abc import ABC

class Person(ABC):
    def getFullName(self):
        pass
    
    def addRequest(self, request):
        pass
    
    def checkRequest(self):
        pass
    
    def addUser(self, user):
        pass
    
class Employee(Person):
    def __init__(self, firstName, lastName, email, department):
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
    
    def getFirstName(self):
        return self._firstName
    
    def setFirstName(self, firstName):
        self._firstName = firstName
    
    def getLastName(self):
        return self._lastName
    
    def setLastName(self, lastName):
        self._lastName = lastName
    
    def getEmail(self):
        return self._email
    
    def setEmail(self, email):
        self._email = email
    
    def getDepartment(self):
        return self._department
    
    def setDepartment(self, department):
        self._department = department
    
    def getFullName(self):
        return f"{self._firstName} {self._lastName}"
        
    def checkRequest(self):
        return "Checked Request!"

    def addUser(self):
        return "User Added!"
    
    def login(self):
        return f"{self._email} has logged in"
    
    def logout(self):
        return f"{self._email} has logged out"

    def addRequest(self):
        return f'Request has been added'

    
class TeamLead(Person):
    def __init__(self, firstName, lastName, email, department):
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

        self._members = []
    
    def getFirstName(self):
        return self._firstName
    
    def setFirstName(self, firstName):
        self._firstName = firstName
    
    def getLastName(self):
        return self._lastName
    
    def setLastName(self, lastName):
        self._lastName = lastName
    
    def getEmail(self):
        return self._email
    
    def setEmail(self, email):
        self._email = email
    
    def getDepartment(self):
        return self._department
    
    def setDepartment(self, department):
        self._department = department
        
    def getFullName(self):
        return f"{self._firstName} {self._lastName}"
        
    def checkRequest(self):
        return "Team lead request checked"
    
    def addUser(self):
        return "Team lead user added"
    
    def login(self):
        return f"{self._email} has logged in"
    
    def logout(self):
        return f"{self._email} has logged out"
    
    def addMember(self, employee):
        self._members.append(employee)

    def getMembers(self):
        return self._members
    
class Admin(Person):
    def __init__(self, firstName, lastName, email, department):
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
    
    def getFirstName(self):
        return self._firstName
    
    def setFirstName(self, firstName):
        self._firstName = firstName
    
    def getLastName(self):
        return self._lastName
    
    def setLastName(self, lastName):
        self._lastName = lastName
    
    def getEmail(self):
        return self._email
    
    def setEmail(self, email):
        self._email = email
    
    def getDepartment(self):
        return self._department
    
    def setDepartment(self, department):
        self._department = department
        
    def getFullName(self):
        return f"{self._firstName} {self._lastName}"
    
    def checkRequest(self):
        return "Admin request checked"
    
    def addRequest(self):
        return "Request Added"
    
    def login(self):
        return f"{self._email} has logged in"
    
    def logout(self):
        return f"{self._email} has logged out"
    
    def addUser(self):
        return "New user added"
    
class Request():
    def __init__(self, name, requester, dateRequested):
        self._name = name
        self._requester = requester
        self._dateRequested = dateRequested
        self._status = "Open"
        
    def updateRequest(self, status):
        self._status = status
        
    def closeRequest(self):
        return f"Request {self._name} has been {self._status}"
        
    def cancelRequest(self, status):
        self._status = "Cancelled"
        return f"{self._status} Request"

    def getStatus(self):
        return self._status

    def getName(self):
        return self._name
    
    def setName(self):
        return self._name
    
    def getRequester(self):
        return self._requester
    
    def setRequester(self):
        return self._requester
    
    def getDateRequested(self):
        return self._dateRequested
    
    def setDateRequested(self):
        return self._dateRequested
    
       

# Test cases:
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.getMembers():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "New user added"

req2.updateRequest("Closed")
print(req2.closeRequest())